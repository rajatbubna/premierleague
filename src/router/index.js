import Vue from 'vue'
import Router from 'vue-router'

const LeagueTeams = resolve => {
  require.ensure(['@/components/home/LeagueTeams'], () => {
    resolve(require('@/components/home/LeagueTeams'))
  }, 'home')
}

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'LeagueTeams',
      component: LeagueTeams
    }
  ]
})
