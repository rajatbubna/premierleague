const state = {
  searchText: ''
}

const getters = {
  searchText: (state) => state.searchText
}

const mutations = {
  SET_SEARCH_TEXT: (state, payload) => {
    state.searchText = payload
  }
}

const actions = {
  setSearchText: ({ commit }, payload) => {
    commit('SET_SEARCH_TEXT', payload)
  }
}

export default{
  state,
  getters,
  mutations,
  actions
}
