import Vue from 'vue'
import Vuex from 'vuex'

import HeaderStore from '@/stores/modules/HeaderStore'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    HeaderStore
  }
})
