import Vue from 'vue'
// The path is relative to the project root.
import LeagueTeams from '@/components/home/LeagueTeams'

describe('LeagueTeams.vue', () => {
  it(`should update when search is changed.`, done => {
    const Constructor = Vue.extend(LeagueTeams)

    const comp = new Constructor().$mount()
    const x = {
      name: 'Arsenal',
      img: 'https://s3-eu-west-1.amazonaws.com/inconbucket/images/entities/original/632.jpg'
    }

    Vue.nextTick(() => {
      expect(comp.filteredTeams[0])
        .to.contain(x)
      // Since we're doing this asynchronously, we need to call done() to tell Mocha that we've finished the test.
      done()
    })
  })
})
